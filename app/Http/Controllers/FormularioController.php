<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormularioController extends Controller {

    public function index(Request $request, $subject){
        if ($request->hasValidSignature()) {
            $num = intval($subject);
            $subject = $num;
            switch($subject) {
                case 1:
                    $titulo = 'Actividad académica';
                break;

                case 2:
                    $titulo = 'Desarrollo laboral';
                break;

                case 3:
                    $titulo = 'Ambiente y lugar de trabajo';
                break;

                case 4:
                    $titulo = 'Asuntos administrativos';
                break;

                case 5:
                    $titulo = 'Proyectos';
                break;

                case 6:
                    $titulo = 'Funciones';
                break;

                case 7:
                    $titulo = 'Comunicación';
                break;

                default:
                    $titulo = 'Otros';
                break;
            }
            return view('formulario.index', ['titulo' => $titulo]);
        } else {
            return view('menu.index');
        }
    }

}