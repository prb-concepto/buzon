@extends('layouts.main')

@section('contenido')
<?php
use Illuminate\Support\Facades\URL;

$subject_academica = URL::signedRoute('form.fill', ['subject' => 1]);
$subject_desarrollo = URL::signedRoute('form.fill', ['subject' => 2]);
$subject_ambiente = URL::signedRoute('form.fill', ['subject' => 3]);
$subject_administrativo = URL::signedRoute('form.fill', ['subject' => 4]);
$subject_proyecto = URL::signedRoute('form.fill', ['subject' => 5]);
$subject_funcion = URL::signedRoute('form.fill', ['subject' => 6]);
$subject_comunicacion = URL::signedRoute('form.fill', ['subject' => 7]);
$subject_otro = URL::signedRoute('form.fill', ['subject' => 8]);

?>
<!-- BEGIN Instrucciones -->
<section class="mbr-section content4 cid-s8P4Tl8wt9" id="content4-4n">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                    Env&iacute;a directamente tu mensaje al titular de la DGTIC.
                    <br>
                    S&oacute;lo &eacute;l podr&aacute; leerlo.
                    <br>
                    Puede ser confidencial e incluso an&oacute;nimo
                </h3>
            </div>
        </div>
    </div>
</section>
<!-- END Instrucciones -->

<!-- BEGIN Pregunta -->
<section class="mbr-section content4 cid-s8P5eGnzO7" id="content4-4o">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                    &iquest;Qu&eacute; tema quieres abordar?
                </h3>
            </div>
        </div>
    </div>
</section>
<!-- END Pregunta -->

<!-- BEGIN MENU -->
<section class="features4 cid-s8IwGB4m8t" id="features4-2l">
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_academica }}" class="text-white">
                                Actividad académica
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_desarrollo }}" class="text-white">
                                Desarrollo laboral
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_ambiente }}" class="text-white">
                                Ambiente y lugar de trabajo
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_administrativo }}" class="text-white">
                                Asuntos administrativos
                            </a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features4 cid-s8IxekS94V" id="features4-2m">
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_proyecto }}" class="text-white">
                                Proyectos
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_funcion }}" class="text-white">
                                Funciones
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_comunicacion }}" class="text-white">
                                Comunicaci&oacute;n
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="card-wrapper media-container-row">
                    <div class="card-box">
                        <h4 class="card-title pb-3 mbr-fonts-style display-5">
                            <a href="{{ $subject_otro }}" class="text-white">
                                Otros
                            </a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END MENU -->
@endsection