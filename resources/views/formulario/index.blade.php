@extends('layouts.main')

@section('contenido')

<?php
if (!isset($titulo)) {
    $titulo = 'Otros';
}
?>

<link rel="stylesheet" href="{{ asset('vendors/formvalidation/dist/css/formValidation.min.css') }}">

<section class="mbr-section content4 cid-s8PaPa58Ko" id="content4-4p">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center pb-3 mbr-fonts-style display-2">
                    {{ $titulo }}
                </h2>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section form1 cid-s8ISdoxy7N" id="form1-2z">
    <div class="container">
        <div class="row justify-content-center">
            <div class="title col-12 col-lg-8"></div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="media-container-column col-lg-8" data-form-type="formoid">
                <form id="frm_buzon" action="#" method="POST" class="mbr-form form-with-styler" data-form-title="Buzón">
                    @csrf
                    <div class="dragArea row">
                        <div class="form-group col-md-12">
                            <small id="apellidosHelpBlock" class="form-text text-muted">
                            Los campos marcados con (<em style="color:red">*</em>) son obligatorios
                            </small>
                        </div>

                        <div class="form-group col-md-12">

                            <label for="frm_nombre">Nombre <em style="color:red">*</em></label>
                            <input type="text" name="frm_nombre" class="form-control" id="frm_nombre" maxlength="80">
                            <small id="nombreHelpBlock" class="form-text text-muted">
                            Ingresa tu nombre o la palabra an&oacute;nimo
                            </small>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="frm_apellidos">Apellidos</label>
                            <input type="text" name="frm_apellidos" class="form-control" id="frm_apellidos" maxlength="80">
                            <small id="apellidosHelpBlock" class="form-text text-muted">
                            Ingresa tus apellidos o la palabra an&oacute;nimo
                            </small>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="frm_correo">Correo UNAM <em style="color:red">*</em></label>
                            <input type="email" name="frm_correo" class="form-control" id="frm_correo" maxlength="255">
                            <small id="correoHelpBlock" class="form-text text-muted">
                            Ingresa tu direcci&oacute;n de correo electr&oacute;nico institucional, debe pertenecer al dominio unam.mx o comunidad.unam.mx
                            </small>
                        </div>

                        <div data-for="comentario" class="col-md-12 form-group">
                            <label for="frm_comentario" class="form-control-label mbr-fonts-style display-7">
                                Comentarios <em style="color:red">*</em>
                            </label>
                            <textarea name="frm_comentario" class="form-control display-7" id="frm_comentario" maxlength="3000" rows="20"></textarea>
                            <small id="comentarioHelpBlock" class="form-text text-muted">
                            Especifica si requieres confidencialidad respecto a este asunto.
                            </small>
                        </div>

                        <div class="col-md-12 input-group-btn align-center">
                            <button type="submit" class="btn btn-primary btn-form display-4">
                                Enviar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js"></script>
<script src="{{ asset('/vendors/formvalidation/dist/js/FormValidation.min.js') }}"></script>
<script src="{{ asset('/vendors/formvalidation/dist/js/plugins/Bootstrap.min.js') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function(e){
        FormValidation.formValidation(
            document.getElementById('frm_buzon'),
            {
                fields: {
                    'frm_nombre': {
                        validators: {
                            notEmpty: {
                                message: 'El campo Nombre es obligatorio.'
                            },

                            stringLength: {
                                min: 3,
                                max: 80,
                                message: 'El campo Nombre debe tener entre 3 y 80 caracteres.'
                            }
                        }
                    },
                    'frm_apellidos': {
                        validators: {
                            stringLength: {
                                min: 3,
                                max: 80,
                                message: 'El campo Apellidos debe tener entre 3 y 80 caracteres.'
                            }
                        }
                    },
                    'frm_correo': {
                        validators: {
                            notEmpty: {
                                message: 'El campo Correo UNAM es obligatorio.'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9._-]+@.*(unam\.mx)$/,
                                message: 'El campo Correo UNAM debe pertenecer al servicio @unam.mx o @comunidad.unam.mx y debe ingresarse en minúsculas.'
                            }
                        }
                    },
                    'frm_comentario': {
                        validators: {
                            notEmpty: {
                                message: 'El campo Comentarios es obligatorio.'
                            },
                            stringLength: {
                                min: 1,
                                max: 3000,
                                message: 'El campo Apellidos debe tener entre 1 y 3000 caracteres.'
                            },
                        },
                    },
                },
                plugins: {
                    message: new FormValidation.plugins.Message(),
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                },
            }
        )
    });
</script>
@endsection